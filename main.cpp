#include "OutStream.h"
#include <iostream>
#include <fstream> //for bonus
#include "Logger.h"

#define KEY 3
#define AGE 1500

void redirect_cout(); //for bonus

int main(int argc, char **argv)
{
	
	//EXT 1

	OutStream o = OutStream(); //creating object

	o << "I am the Doctor and I'm ";
	o << AGE;
	o << " years old";
	o.endline();

	//EXT 2
	FileStream f = FileStream("ex2_2 test.txt"); //creating FileStream, there's a .txt file called "ex2_2 test" in my solution
	f << "I am the Doctor and I'm ";
	f << AGE;
	f << " years old";
	f.endline();
	
	//EXT3
	OutStreamEncrypted o1 = OutStreamEncrypted(KEY); //TODO: DEFINE 3
	o1 << "I am the Doctor and I'm ";
	o1 << AGE;
	o1 << " years old\n";
	

	//EXT4
	Logger l = Logger();
	l << "Happy\n";
	l << 44;
	l << " Birthday!\n";
	

	//EXT5
	Logger l2 = Logger();
	l2 << "And hello to\n";
	l2 << 20;
	l2 << " also!";

	//BONUS
	redirect_cout();


	//std::cout << "some print";
	//^ this will not work, memory access error. 
	/*
		we can add a backup streamBuff of cout and then
		in the end set it back to the stream buffer of cout (and then there will be no error)
		but it will violate the instruction of the exercise... 
		

		std::streambuf* coutBackup = std::cout.rdbuf();
		std::cout << "some print";
		std::cout.rdbuf(coutBackup);

		I hope it counts :D

	*/
	
	getchar();
	return 0;
}

/*
	the function redirects all outputs to cout to a file
	input & output: none
*/
void redirect_cout()
{
	std::streambuf* fileStreamBuff = 0;
	std::ofstream fp = std::ofstream(); //the log.txt file we are going to open. 
									   //type = std::ofstream, because it's a output file stream

	fp.open("log.txt",std::ios::out); //opening the file for output

	fileStreamBuff = fp.rdbuf(); //assigning fileStreamBuff to the stream buff of the file
	
	std::cout.rdbuf(fileStreamBuff); //setting fileStreamBuff to be the stream buffer of cout
	
	//std::cout << "hello world"; //this works, but not outside of the function...

	fp.close();
	
	
}
