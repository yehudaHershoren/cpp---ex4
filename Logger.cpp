#include "Logger.h"


Logger::Logger()
{
	this->_startLine = true; //default, first print is always a new line
	this->os = OutStream();
}

Logger::~Logger()
{
 //no dynamically allocated fields in Logger
}


void Logger::setStartLine()
{
	this->_startLine = !this->_startLine;
	static unsigned int countLog = 0;
	if (!this->_startLine)
	{
		countLog++;
		fprintf(stdout, "%x: ",countLog);
	}
	
}




Logger& operator<<(Logger& l, std::string msg)
{
	int i = 0;

	if (l._startLine) // if it's the start of a new line 
	{
		fprintf(stdout, "%s", "LOG "); 
		l.setStartLine();
	}

	for (i = 0; i < msg.length(); i++)
	{
		fprintf(stdout, "%c", msg[i]); //printing char
		if (msg[i] == '\n') //if the char is a new line
		{
			l.setStartLine();

			if (i != msg.length() - 1) //if its not the last char in msg. (the message is not over!)
			{
				fprintf(stdout, "%s", "LOG ");
			}

		}
	}
	
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	if (l._startLine)
	{
		fprintf(stdout, "%s", "LOG "); 
		l.setStartLine();
	}
	l.os << num;
	return l;
}