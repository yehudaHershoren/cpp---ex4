#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define WRITE_FILE "w"
#define MIN_ASCII 32
#define MAX_ASCII 126
#include <iostream> //only for FILE*

namespace msl
{


	class OutStream
	{
	protected:
		FILE * _fp;

	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
		void endline(); //changing endline(), so classes that inhert to OutStream can recognize it
	};




	class FileStream : OutStream
	{

	public:
		FileStream(const char* name); //constructor
		~FileStream(); //destructor
		FileStream& operator<<(const char *str); // << operator, writing a string to the file
		FileStream& operator<<(int num);		 // << operator, writing a number to the file
		void endline();	//writing a new line to the file
	};


	class OutStreamEncrypted : OutStream
	{
	private:
		unsigned int key; // key of the caesar cipher
		char getEncryptedChar(char c); //encrypts one character

	public:
		OutStreamEncrypted(unsigned int key); //constrcutor
		OutStreamEncrypted& operator<<(const char *str);
		OutStreamEncrypted& operator<<(int num);
		//we don't need to run over endline(), because '\n' is out of the ascii range ('\n'=0xA) 
	};

}