#pragma once

#include "OutStream.h"
#include <stdio.h>

class Logger
{
	OutStream os;
	bool _startLine;
	void setStartLine();
public:
	Logger();
	~Logger();
	friend Logger& operator<<(Logger& l, std::string msg);
	friend Logger& operator<<(Logger& l, int num);
	Logger& operator<<(void(*pf)());
};

