#include "OutStream.h"
#include <stdio.h>
#include <string> //std::to_string


OutStream::OutStream()
{	
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(stdout,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num) 
{
	fprintf(stdout, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void OutStream::endline()
{
	fprintf(stdout,"\n");
}


FileStream::FileStream(const char* name)
{
	_fp = fopen(name, WRITE_FILE);
}

FileStream::~FileStream()
{
	fclose(_fp);
	_fp = nullptr;
}

FileStream& FileStream::operator<<(const char *str)
{
	fprintf(_fp,"%s",str);
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	fprintf(_fp, "%d", num);
	return *this;
}

void FileStream::endline()
{
	fprintf(_fp, "\n");
}


OutStreamEncrypted::OutStreamEncrypted(unsigned int key)
{
	this->key = key;
}

char OutStreamEncrypted::getEncryptedChar(char c)
{
	char result = 0;

	if (c >= MIN_ASCII && c <= MAX_ASCII) //making sure the char is in range
	{
		if (c + key > MAX_ASCII)
		{
			result = (char)(MIN_ASCII + (MAX_ASCII - c)); //sycling back to the minimum value when the encrypted value is out of range
		}
		else
		{
			result = (char)(c + key); 
		}
		
	}
	else
	{
		result = c; //if char is not in range, it remains the same
	}

	return result;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	int i = 0;

	for (i = 0; i < strlen(str); i++)
	{
		fprintf(stdout,"%c",getEncryptedChar(str[i])); //printing each time one encrypted character
	}
	return *this;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int i = 0;
	std::string strNum = "";
	strNum = std::to_string(num); //converting the number to string
	
	for (i = 0; i < strNum.length(); i++)
	{
		fprintf(stdout, "%c", getEncryptedChar(strNum[i])); //printing the encrypted value for a digit
	}
	
	
	return *this;
}
